#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  double Ellipse::Perimeter() const
  {
      double perimetro = 2 * M_PI *sqrt((_a * _a + _b * _b)/2);
      return perimetro;
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    double latoA = sqrt((pow(points[0].X - points[1].X, 2.0)) + (pow(points[0].Y - points[1].Y, 2.0)));
    double latoB = sqrt((pow(points[1].X - points[2].X, 2.0)) + (pow(points[1].Y - points[2].Y, 2.0)));
    double latoC = sqrt((pow(points[2].X - points[0].X, 2.0)) + (pow(points[2].Y - points[0].Y, 2.0)));
    perimeter = latoA + latoB + latoC;

    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,
                                           const double& edge)
  {
    points.resize(3);
    points[0] = p1;
    points[1] = Point(points[0].X + (1.0/2.0) * edge, points[0].Y - (sqrt(3.0)/2.0) * edge);
    points[2] = Point(points[0].X - (1.0/2.0) * edge , points[0].Y - (sqrt(3.0)/2.0) * edge );
  }



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
    points.reserve(4);
    points[0] = p1;
    points[1] = p2;
    points[2] = p3;
    points[3] = p4;
  }

  void Quadrilateral::AddVertex(const Point &p)
  {
    int numeroVertici = points.size();
    int flag = 0;
    for (int i = 0; i < numeroVertici && flag == 0; i++)
    {
        if(points[i].X == 0.0 && points[i].Y == 0.0)
        {
            points[i] = p;
            flag = 1;
        }
    }

  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    double latoA = sqrt((pow(points[0].X - points[1].X, 2.0)) + (pow(points[0].Y - points[1].Y, 2.0)));
    double latoB = sqrt((pow(points[1].X - points[2].X, 2.0)) + (pow(points[1].Y - points[2].Y, 2.0)));
    double latoC = sqrt((pow(points[2].X - points[3].X, 2.0)) + (pow(points[2].Y - points[3].Y, 2.0)));
    double latoD = sqrt((pow(points[3].X - points[0].X, 2.0)) + (pow(points[3].Y - points[0].Y, 2.0)));
    perimeter = latoA + latoB + latoC + latoD;
    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,
                       const double& base,
                       const double& height)
  {
    points.resize(4);
    points[0] = p1;
    points[1] = Point(points[0].X + base, points[0].Y);
    points[3] = Point(points[0].X , points[0].Y - height);
    points[2] = Point(points[0].X + base, points[0].Y - height);
  }


  Point Point::operator+(const Point& point) const
  {
      return Point(X + point.X, Y + point.Y);
  }

  Point Point::operator-(const Point& point) const
  {
      return Point(X - point.X, Y - point.Y);
  }

  Point&Point::operator-=(const Point& point)
  {
      *this = *this - point;
      return *this;
  }

  Point&Point::operator+=(const Point& point)
  {
      *this = *this + point;
      return *this;
  }

  Square::Square(const Point &p1, const double &edge)
  {
      points.resize(4);
      points[0] = p1;
      points[1] = Point(points[0].X + edge, points[0].Y);
      points[3] = Point(points[0].X , points[0].Y - edge);
      points[2] = Point(points[0].X + edge, points[0].Y - edge);
  }

}
