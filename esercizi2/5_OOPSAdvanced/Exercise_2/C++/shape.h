#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
#include <sstream>
using namespace std;

namespace ShapeLibrary

  class Point {
    public:
      double X;
      double Y;
      Point() {X = 0.0; Y = 0.0;}
      Point(const double& x, const double& y) {X = x; Y = y; }
      Point(const Point& point) { *this = point; }
     // virtual ~Point() { }
      Point& operator=(const Point& point){
          X = point.X;
          Y = point.Y;
          return *this;
      }
      double ComputeNorm2() const { double norma = sqrt(X*X + Y*Y);    return norma;}
      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point)
      {
          stringstream ss, ss2;
          ss << point.X;
          ss2 << point.Y;
          stream << "Point: x = " + ss.str() + " y = " + ss2.str() + "\n";
          return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() < rhs.Perimeter(); }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() > rhs.Perimeter(); }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() <= rhs.Perimeter(); }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){ return lhs.Perimeter() >= rhs.Perimeter(); }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() {_center = Point(0.0,0.0); _a = 0.0; _b = 0.0;}
      Ellipse(const Point& center,
              const double& a,
              const double& b): _center(center){ _a = a; _b = b; }
      virtual ~Ellipse() { }
      void AddVertex(const Point& point) { _center = point; }
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle() { _center = Point(0.0, 0.0); _a = 0.0; _b = 0.0; }
      Circle(const Point& center,
             const double& radius) { _center = center; _a = radius; _b = radius; }
      virtual ~Circle() { }
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle() {points.reserve(3); points[0] = Point(0.0,0.0); points[1] = Point(0.0,0.0); points[2] = Point(0.0,0.0); }
      Triangle(const Point &p1, const Point &p2, const Point &p3)
      {
          points.resize(3);
          points[0] = p1;
          points[1] = p2;
          points[2] = p3;
      }
      //virtual ~Triangle() { }

      void AddVertex(const Point& point) { int numeroVertici = points.size(); points[numeroVertici] = point;}
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const double& edge);
      virtual ~TriangleEquilateral() { }
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral() {points.resize(4); points[0] = Point(0.0,0.0); points[1] = Point(0.0,0.0); points[2] = Point(0.0,0.0);
                       points[3] = Point(0.0, 0.0);}
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p);

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
    {
      public:
        Rectangle() {points.resize(4); points[0] = Point(0.0,0.0); points[1] = Point(0.0,0.0); points[2] = Point(0.0,0.0);
                     points[3] = Point(0.0, 0.0);}
        Rectangle(const Point& p1,
                  const Point& p2,
                  const Point& p3,
                  const Point& p4) {
            points.resize(4);
            points[0] = p1;
            points[1] = p2;
            points[2] = p3;
            points[3] = p4;
        }
        Rectangle(const Point& p1,
                  const double& base,
                  const double& height);
        virtual ~Rectangle() { }
    };


  class Square: public Rectangle
  {
    public:
      Square() {points.resize(4); points[0] = Point(0.0,0.0); points[1] = Point(0.0,0.0); points[2] = Point(0.0,0.0);
                points[3] = Point(0.0, 0.0); }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4) {
          points.resize(4);
          points[0] = p1;
          points[1] = p2;
          points[2] = p3;
          points[3] = p4;
      }
      Square(const Point& p1,
             const double& edge);
      virtual ~Square() { }
  };
}

#endif // SHAPE_H
