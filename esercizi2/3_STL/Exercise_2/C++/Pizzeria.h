#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <unordered_map>
#include <stack>
#include <queue>
#include<sstream>
using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
      public:
      Ingredient(const string& name, const int& price, const string& description){Name = name; Price = price; Description = description; }
      //Ingredient(const Ingredient& ingredient){ }
      Ingredient& operator =(const Ingredient& ingredient){
          Name = ingredient.Name;
          Price = ingredient.Price;
          Description = ingredient.Description;
          return *this;
      }

  };

  class Pizza {
    public:
      string Name;
    private:
      queue<Ingredient> ingredients;
    public:
      Pizza(const string& name, queue<Ingredient> ingredienti){
          Name = name;
          unsigned int numIngredienti = ingredienti.size();
          for(unsigned int i = 0; i < numIngredienti; i++)
          {
              Ingredient appoggio = ingredienti.front();
              ingredients.push(appoggio);
              ingredienti.pop();

          }


      }

      void AddIngredient(const Ingredient& ingredient) { ingredients.push(ingredient); }
      int NumIngredients() const { return ingredients.size(); }
      int ComputePrice() const {
          unsigned int numIngredients = NumIngredients();
          unsigned int price = 0;
          queue<Ingredient> ingredients_localCopy = ingredients;
          for(unsigned int i = 0; i < numIngredients; i++)
          {
              Ingredient ingredient = ingredients_localCopy.front();
              price = price + ingredient.Price;
              ingredients_localCopy.pop();

          }
          return price;

      }
  };

  class Order {
  private:
      vector<Pizza> listaPizze;
      int numOrdine;
  public:
      void InitializeOrder(int numPizzas, int numerOrdine) { listaPizze.reserve(numPizzas); numOrdine = numerOrdine; }
      void AddPizza(const Pizza& pizza) { listaPizze.push_back(pizza); }
      const Pizza& GetPizza(const int& position) const {
          if(position >= listaPizze.size() || position == 0)
              throw runtime_error("Position passed is wrong");
          else
              return listaPizze[position-1];
      }
      int NumPizzas() const { return listaPizze.size(); }
      int ComputeTotal() const {
          unsigned int numpizze = NumPizzas();
          unsigned int price = 0;
          for( unsigned int i = 0; i < numpizze; i++)
          {
              Pizza pizza = listaPizze[i];
              price += pizza.ComputePrice();
          }
          return price;
      }
  };

  class Pizzeria {

  private:
      unordered_map<string,Ingredient> listaIngredienti;
      unordered_map<string,Pizza> menu;
      vector<Order> listaOrdini;

  public:
      void AddIngredient(const string& name, const string& description, const int& price) {
          Ingredient lastIngredient = Ingredient(name, price, description);
          if(!listaIngredienti.emplace(name,lastIngredient).second)
             throw runtime_error("Ingredient already inserted");
          else
             listaIngredienti.emplace(name,lastIngredient);
      }

      const Ingredient& FindIngredient(const string& name) const {
          //unordered_map<string,Ingredient>::const_iterator it = listaIngredienti.find(name);
          std::unordered_map<std::string,Ingredient>::const_iterator it = listaIngredienti.find(name);
          if(it == listaIngredienti.end())
              throw runtime_error("Ingredient not found");
          else
              return it->second;
      }
      void AddPizza(const string& name,const vector<string>& ingredients) {
          unsigned int numIngredients = ingredients.size();
          queue<Ingredient> ingredientiPizza;

          for( unsigned int i = 0; i < numIngredients; i++ )
              ingredientiPizza.push(FindIngredient(ingredients[i]));
          Pizza nuovaPizza = Pizza(name, ingredientiPizza);
          if(!menu.emplace(name,nuovaPizza).second)
             throw runtime_error("Pizza already inserted");
          else
             menu.emplace(name,nuovaPizza);
      }
      const Pizza& FindPizza(const string& name) const {
          std::unordered_map<std::string,Pizza>::const_iterator it = menu.find(name);
          if(it == menu.end())
              throw runtime_error("Pizza not found");
          else
              return it->second;
      }
      int CreateOrder(const vector<string>& pizzas) {
          int numerOrdine = listaOrdini.size() + 1000;
          int numeroPizze = pizzas.size();
          if(numeroPizze == 0)
              throw runtime_error("Empty order");
          listaOrdini.reserve(numerOrdine-1000);
          listaOrdini[numerOrdine-1000].InitializeOrder(numeroPizze, numerOrdine);
          for(unsigned int i = 0; i < numeroPizze; i++)
          {
              listaOrdini[i].AddPizza(FindPizza(pizzas[i]));
          }
          return numerOrdine;
      }
      const Order& FindOrder(const int& numOrder) const {
          if((numOrder >= listaOrdini.size() + 1000) || numOrder < 1000)
              throw runtime_error("Position passed is wrong");
          else
              return listaOrdini[numOrder];

      }
      string GetReceipt(const int& numOrder) const {
          //for each pizza in the order "- " + name + ", " + price + " euro" + "\n" " TOTAL: " + total + " euro" + "\n"
          string scontrino;
          Order ordine = FindOrder(numOrder);
          for(int i = 0; i < ordine.NumPizzas(); i++)
          {
              Pizza pizzadascrivere = ordine.GetPizza(i);

              stringstream ss, ss2;
              int prezzo = pizzadascrivere.ComputePrice();
              ss << prezzo;
              string str = ss.str();
              int totale = ordine.ComputeTotal();
              ss2 << totale;
              string str2 = ss2.str();
              scontrino = scontrino + "- " + pizzadascrivere.Name + ", " + str + " euro" + "\n" "TOTAL: " + str2 + " euro" + "\n";
          }
          return scontrino;
      }
      string ListIngredients() const {
          string ingredientiLista;
          for (auto it = listaIngredienti.begin(); it != listaIngredienti.end(); ++it )
          {
              stringstream ss;
              ss << it->second.Price;
              string str = ss.str();
              ingredientiLista = ingredientiLista + it->first + " - '" + it->second.Description + "': " + str + " euro" + "\n";
          }
          return ingredientiLista;
      }
      string Menu() const {
          string tutteLePizze;
          for (auto it = menu.begin(); it != menu.end(); ++it )
          {
              stringstream ss, ss2;
              int numeroIngredienti = it->second.NumIngredients();
              ss << numeroIngredienti;
              string str = ss.str();
              ss2 << it->second.ComputePrice();
              string str2 = ss2.str();
              tutteLePizze = tutteLePizze + it->first + " (" + str + " ingredients): " + str2 + " euro" + "\n";
          }
          return tutteLePizze;
      }
  };
};

#endif // PIZZERIA_H
