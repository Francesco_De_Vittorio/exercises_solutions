class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description

class Pizza:
    def __init__(self, name: str, ingredients: []):
        self.Name = name
        self.Ingredients = ingredients

    def addIngredient(self, ingredient: Ingredient):
        self.Ingredients.append(ingredient)
        return None

    def numIngredients(self) -> int:
        return len(self.Ingredients)

    def computePrice(self) -> int:
        price = 0
        tmpList = self.Ingredients
        for i in self.Ingredients:
            price += i.Price
        return price

class Order:
    def __init__(self, numorder: int):
        self.pizzas = []
        self.numOrder = numorder

    def getPizza(self, position: int) -> Pizza:
        if position >= len(self.pizzas) or position == 0:
            raise Exception("Position passed is wrong")
        else:
            i = -1
            for it in self.pizzas:
                i = i + 1
                if i == position - 1:
                    return it

    def initializeOrder(self, numPizzas: int):
        return None

    def addPizza(self, pizza: Pizza):
        self.pizzas.append(pizza)
        return None

    def numPizzas(self) -> int:
        return len(self.pizzas)

    def computeTotal(self) -> int:
        price = 0
        for i in self.pizzas:
            price += Pizza.computePrice(i)
        return price


class Pizzeria:
    def __init__(self):
        self.ingredients = []
        self.pizzas = []
        self.orders = []

    def addIngredient(self, name: str, description: str, price: int):
        flag = 0
        for it in self.ingredients:
            if it.Name == name:
                flag = 1
        if flag == 1:
            raise Exception("Ingredient already inserted")
        else:
            newIngredient = Ingredient(name, price, description)
            self.ingredients.append(newIngredient)

    def findIngredient(self, name: str) -> Ingredient:
        for it in self.ingredients:
            if it.Name == name:
                return it
        raise Exception("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        for it in self.pizzas:
            if name == it.Name:
                raise Exception("Pizza already inserted")
        tmpList = []
        for i in ingredients:
            tmpList.append(self.findIngredient(i))
        newPizza = Pizza(name, tmpList)
        self.pizzas.append(newPizza)
        return None

    def findPizza(self, name: str) -> Pizza:
        for it in self.pizzas:
            if it.Name == name:
                return it
        raise Exception("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        if len(pizzas) == 0:
            raise Exception("Empty order")
        dimOrder = len(self.orders)
        newOrder = Order(1000 + dimOrder)
        for i in range(0, len(pizzas)):
            tmpPizza = self.findPizza(pizzas[i])
            newOrder.pizzas.append(tmpPizza)
        self.orders.append(newOrder)
        return newOrder.numOrder

    def findOrder(self, numOrder: int) -> Order:
        for it in self.orders:
            if it.numOrder == numOrder:
                return it
        raise Exception("Order not found")

    def getReceipt(self, numOrder: int) -> str:
        receipt = ""
        total_ = 0
        fOrder = self.findOrder(numOrder)
        i = 0
        for it in fOrder.pizzas:
            ingPrice = 0
            fPizza = self.findPizza(it.Name)
            for iter in fPizza.Ingredients:
                fIngredient = self.findIngredient(iter.Name)
                ingPrice = ingPrice + fIngredient.Price
            if i == 0:
                receipt = "- " + fPizza.Name + ", " + str(ingPrice) + " euro" + "\n"
            else:
                receipt = receipt + "- " + fPizza.Name + ", " + str(ingPrice) + " euro" + "\n"
            total_ = total_ + ingPrice
            i = i + 1
        receipt = receipt + " TOTAL: " + str(total_) + " euro" + "\n"
        return receipt

    def listIngredients(self) -> str:
        nameIngredients = []
        ordList = ""
        for it in self.ingredients:
            nameIngredients.append(it.Name)
        nameIngredients.sort()
        for i in nameIngredients:
            for it in self.ingredients:
                if i == it.Name:
                    if i == 0:
                        ordList = it.Name + " - '" + it.Description + "': " + str(it.Price) + " euro" + "\n"
                    else:
                        ordList = ordList + it.Name + " - '" + it.Description + "': " + str(it.Price) + " euro" + "\n"

        return ordList

    def menu(self) -> str:
        i = -1
        for it in self.pizzas:
            i = i + 1
            newPizza = it
            numberIngredients = Pizza.numIngredients(newPizza)
            pricePizza = Pizza.computePrice(newPizza)
            if i == 0:
                menu = it.Name + " (" + str(numberIngredients) + " ingredients): " + str(pricePizza) + " euro" + "\n"
            else:
                menu = menu + it.Name + " (" + str(numberIngredients) + " ingredients): " + str(
                    pricePizza) + " euro" + "\n"

        return menu

