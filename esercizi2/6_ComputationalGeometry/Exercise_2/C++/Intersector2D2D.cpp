#include "Intersector2D2D.h"

// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
   toleranceParallelism = 1.0E-7;
   toleranceIntersection = 1.0E-7;
   pointLine.setZero(3);
   tangentLine.setZero(3);
}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
   matrixNomalVector.row(0) = planeNormal;
   rightHandSide[0] = planeTranslation;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
   matrixNomalVector.row(1) = planeNormal;
   rightHandSide[1] = planeTranslation;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
  tangentLine = matrixNomalVector.row(0).cross(matrixNomalVector.row(1));
  double parallelism = tangentLine.squaredNorm();
  double check = toleranceParallelism * toleranceParallelism * matrixNomalVector.row(0).squaredNorm() * matrixNomalVector.row(1).squaredNorm();
  if ( parallelism * parallelism <= check )
  {
     if ( abs(rightHandSide[0] - rightHandSide[1]) < toleranceIntersection )
     {
         intersectionType = Coplanar;
         return false;
     }
     else
         intersectionType = NoInteresection;
     return false;
  }
  else
  {
      matrixNomalVector.row(2) = tangentLine;
      rightHandSide[2] = 0.00;
      Vector3d pointLine = matrixNomalVector.fullPivLu().solve(rightHandSide);
      intersectionType = LineIntersection;
      return true;
  }





















}
