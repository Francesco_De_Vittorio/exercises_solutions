#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
  toleranceParallelism = 1.0E-7;
  toleranceIntersection = 1.0E-7;
  intersectionType = NoInteresection;
}
Intersector2D1D::~Intersector2D1D()
{

}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
  planeNormalPointer = planeNormal;
  planeTranslationPointer = planeTranslation;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
  lineOriginPointer = lineOrigin;
  lineTangentPointer = lineTangent;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
  double utilePerCheck = planeNormalPointer.dot(lineOriginPointer);
  double utilePerCheck2 = planeNormalPointer.dot(lineTangentPointer);
  if(abs(utilePerCheck2) < toleranceParallelism)
  {
      if((planeTranslationPointer > utilePerCheck - toleranceParallelism) && (planeTranslationPointer < utilePerCheck + toleranceParallelism))
      {
          intersectionType = Coplanar;
          return false;
      }
      else
      {
          intersectionType = NoInteresection;
          return false;
      }
  }
  else
  {
      intersectionParametricCoordinate = (planeTranslationPointer - utilePerCheck) / utilePerCheck2;
      intersectionType = PointIntersection;
  }
  return true;
}
