```plantuml
@startuml

package "Data Transfer Object (DTO)" <<Database>> {
  class Bus {
    +Id: int
    +FuelCost: int
  }

  class BusStop {
    +Id: int
    +Latitude: int
    +Longitude: int
    +Name: string
  }

  class Street {
    +Id: int
    +TravelTime: int
  }

  class Route {
    +Id: int
    +NumberStreets: int
  }
}

package "Business Logic" {
  interface IBusStation {
    +void Load()
    +int NumberBuses()
    +Bus GetBus(int idBus)
  }

  interface IMapData{
    +void Load()
    +int NumberRoutes()
    +int NumberStreets()
    +int NumberBusStops()
    +Route GetRoute(int idRoute)
    +Street GetRouteStreet(int idRoute, int streetPosition)
    +Street GetStreet(int idStreet)
    +BusStop GetStreetFrom(int idStreet)
    +BusStop GetStreetTo(int idStreet)
    +BusStop GetBusStop(int idBusStop)
  }

  interface IRoutePlanner {
    +int ComputeRouteTravelTime(int idRoute)
    +int ComputeRouteCost(int idBus, int idRoute)
  }

  interface IMapViewer {
    +string ViewRoute(int idRoute)
    +string ViewStreet(int idStreet)
    +string ViewBusStop(int idBusStop)
  }
}

IMapViewer ..> IMapData : depends
IRoutePlanner ..> IMapData : depends
IRoutePlanner ..> IBusStation : depends
IMapData "1" *-- "*" Route : contains
IMapData "1" *-- "*" Street : contains
IMapData "1" *-- "*" BusStop : contains
IBusStation "1" *-- "*" Bus : contains

@enduml
```