#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    _x = x;
    _y = y;
}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3) : _p1(p1), _p2(p2), _p3(p3)
{
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
}

double Triangle::Area() const
{
    return 0.5*abs(_p1._x * _p2._y + _p2._x * _p3._y + _p3._x * _p1._y - _p2._x * _p1._y - _p3._x * _p2._y - _p1._x * _p3._y);
}

double TriangleEquilateral::Area() const
{
    double Area;
    Area = (sqrt(3)/4) * _edge * _edge;
    return Area;
}

Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4):  _p1(p1), _p2(p2), _p3(p3), _p4(p4)
{
    _p1 = p1;
    _p2 = p2;
    _p3 = p3;
    _p4 = p4;
}

double Quadrilateral::Area() const
{
    double Area;
    Area = 0.5*abs(_p1._x * _p2._y + _p2._x * _p3._y + _p3._x * _p4._y + _p4._x * _p1._y - _p2._x * _p1._y - _p3._x * _p2._y - _p4._x * _p3._y - _p1._x * _p4._y);
    return Area;
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4): _p1(p1), _p2(p2), _p4(p4)
{
    _p1 = p1;
    _p2 = p2;
    _p4 = p4;
}

double Parallelogram::Area() const
{
    double Area;
    Area = abs(_p1._x * _p2._y + _p2._x * _p4._y + _p4._x * _p1._y - _p2._x * _p1._y - _p4._x * _p2._y - _p1._x * _p4._y);
    return Area;
}










}
